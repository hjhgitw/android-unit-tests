package com.oliverspryn.blog.androidunittests.details

import com.oliverspryn.blog.androidunittests.mvc.ObservableViewMvc

interface DetailsViewMvc : ObservableViewMvc<DetailsViewMvc.Listener> {
    interface Listener {
        fun onPreviewTap(uri: String)
    }

    fun setPhoto(uri: String)
    fun setTitle(titleText: String)
}
