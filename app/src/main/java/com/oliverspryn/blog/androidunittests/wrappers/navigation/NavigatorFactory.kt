package com.oliverspryn.blog.androidunittests.wrappers.navigation

import android.content.Context
import com.oliverspryn.blog.androidunittests.navigation.navigators.HttpNavigator
import com.oliverspryn.blog.androidunittests.wrappers.android.AlertDialogBuilderFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.IntentFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import javax.inject.Inject

class NavigatorFactory @Inject constructor() {

    fun newHttpNavigatorInstance(
        alertDialogBuilderFactory: AlertDialogBuilderFactory,
        context: Context,
        intentFactory: IntentFactory,
        uriForwarder: UriForwarder
    ) = HttpNavigator(
        alertDialogBuilderFactory = alertDialogBuilderFactory,
        context = context,
        intentFactory = intentFactory,
        uriForwarder = uriForwarder
    )
}
