package com.oliverspryn.blog.androidunittests.dagger.modules

import android.content.Context
import com.oliverspryn.blog.androidunittests.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainModule(
    private val activity: MainActivity
) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideMainActivity() = activity
}
