package com.oliverspryn.blog.androidunittests

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.oliverspryn.blog.androidunittests.dagger.DaggerInjector
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewMvcFactory: ViewMvcFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerInjector.buildMainComponent(this).inject(this)

        val viewMvc = viewMvcFactory.getMainViewMvc(parent = null)
        setContentView(viewMvc.rootView)
    }
}
