package com.oliverspryn.blog.androidunittests.wrappers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerForwarder {
    val io: Scheduler by lazy { Schedulers.io() }
    val mainThread: Scheduler by lazy { AndroidSchedulers.mainThread() }
}
