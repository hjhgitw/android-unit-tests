package com.oliverspryn.blog.androidunittests.photos

import io.reactivex.Single
import retrofit2.http.GET

interface PhotosApiService {
    @GET("/photos")
    fun getPhotos(): Single<List<PhotosModel>>
}
