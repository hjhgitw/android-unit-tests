package com.oliverspryn.blog.androidunittests.dagger.components

import com.oliverspryn.blog.androidunittests.MainActivity
import com.oliverspryn.blog.androidunittests.dagger.modules.*
import com.oliverspryn.blog.androidunittests.details.DetailsFragment
import com.oliverspryn.blog.androidunittests.navigation.CustomNavHostFragment
import com.oliverspryn.blog.androidunittests.photos.PhotosFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    HttpServiceModule::class,
    MainModule::class,
    MvcModule::class,
    RxJavaModule::class,
    WrappersModule::class
])
interface MainComponent {
    fun inject(customNavHostFragment: CustomNavHostFragment)
    fun inject(detailsFragment: DetailsFragment)
    fun inject(mainActivity: MainActivity)
    fun inject(photosFragment: PhotosFragment)
}
