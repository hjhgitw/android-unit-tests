package com.oliverspryn.blog.androidunittests.details

import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavDirectionsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigationForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import javax.inject.Inject

class DetailsController @Inject constructor(
    private val navDirectionsForwarder: NavDirectionsForwarder,
    private val navigationForwarder: NavigationForwarder,
    private val uriForwarder: UriForwarder
) : DetailsViewMvc.Listener {

    var viewMvc: DetailsViewMvc? = null

    fun onCreateView(model: PhotosModel) {
        viewMvc?.setPhoto(model.url)
        viewMvc?.setTitle(model.title)
    }

    fun onStart() {
        viewMvc?.registerListener(listener = this)
    }

    fun onDestroy() {
        viewMvc?.unregisterListener(listener = this)
    }

    // region DetailsViewMvc.Listener

    override fun onPreviewTap(uri: String) {
        viewMvc?.rootView?.let {
            val convertedUri = uriForwarder.parse(uri)
            val directions = navDirectionsForwarder.actionDetailsFragmentToPreviewPictureUrl(convertedUri)

            navigationForwarder
                .findNavController(it)
                .navigate(directions)
        }
    }

    // endregion
}
