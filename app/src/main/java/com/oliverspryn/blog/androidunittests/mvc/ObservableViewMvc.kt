package com.oliverspryn.blog.androidunittests.mvc

interface ObservableViewMvc<Listener> : ViewMvc {
    fun registerListener(listener: Listener)
    fun unregisterListener(listener: Listener)
}
