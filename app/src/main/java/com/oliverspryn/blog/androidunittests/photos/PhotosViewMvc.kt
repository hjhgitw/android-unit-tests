package com.oliverspryn.blog.androidunittests.photos

import com.oliverspryn.blog.androidunittests.mvc.ViewMvc

interface PhotosViewMvc : ViewMvc {
    fun setAdapter(adapter: PhotosAdapter)
}
