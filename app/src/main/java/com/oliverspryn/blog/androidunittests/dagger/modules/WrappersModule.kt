package com.oliverspryn.blog.androidunittests.dagger.modules

import com.oliverspryn.blog.androidunittests.wrappers.android.AlertDialogBuilderFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.IntentFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.LinearLayoutManagerFactory
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavArgsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavDirectionsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigationForwarder
import com.oliverspryn.blog.androidunittests.wrappers.PhotosFactory
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.ToastForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WrappersModule {

    @Provides
    @Singleton
    fun provideAlertDialogBuilderFactory() = AlertDialogBuilderFactory()

    @Provides
    @Singleton
    fun provideIntentFactory() = IntentFactory()

    @Provides
    @Singleton
    fun provideLinearLayoutManagerFactory() = LinearLayoutManagerFactory()

    @Provides
    @Singleton
    fun provideNavArgsForwarder() = NavArgsForwarder()

    @Provides
    @Singleton
    fun provideNavDirectionsForwarder() = NavDirectionsForwarder()

    @Provides
    @Singleton
    fun provideNavigationForwarder() = NavigationForwarder()

    @Provides
    @Singleton
    fun providePhotosAdapterViewHolderFactory() = PhotosFactory()

    @Provides
    @Singleton
    fun providePicassoForwarder() = PicassoForwarder()

    @Provides
    @Singleton
    fun provideToastForwarder() = ToastForwarder()

    @Provides
    @Singleton
    fun provideUriForwarder() = UriForwarder()
}
