package com.oliverspryn.blog.androidunittests.photos.photolistitem

import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.oliverspryn.blog.androidunittests.mvc.ObservableViewMvc

interface PhotoListItemViewMvc : ObservableViewMvc<PhotoListItemViewMvc.Listener> {
    interface Listener {
        fun onPhotoTap(photoModel: PhotosModel)
    }

    fun bindPhoto(photoModel: PhotosModel)
}
