package com.oliverspryn.blog.androidunittests.wrappers.navigation

import android.os.Bundle
import com.oliverspryn.blog.androidunittests.details.DetailsFragmentArgs

class NavArgsForwarder {
    fun detailsFragmentArgs(bundle: Bundle) = DetailsFragmentArgs.fromBundle(bundle)
}
