package com.oliverspryn.blog.androidunittests.wrappers.android

import android.content.Intent
import android.net.Uri

class IntentFactory {
    fun newInstance(action: String, uri: Uri) = Intent(action, uri)
}
