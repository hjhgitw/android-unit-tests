package com.oliverspryn.blog.androidunittests.photos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oliverspryn.blog.androidunittests.dagger.DaggerInjector
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.LinearLayoutManagerFactory
import javax.inject.Inject

class PhotosFragment(
    mainComponent: MainComponent? = DaggerInjector.mainComponent
) : Fragment() {

    @Inject
    lateinit var linearLayoutManagerFactory: LinearLayoutManagerFactory

    @Inject
    lateinit var photosController: PhotosController

    @Inject
    lateinit var viewMvcFactory: ViewMvcFactory

    init {
        mainComponent?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewMvc = viewMvcFactory.getPhotosViewMvc(linearLayoutManagerFactory, container)
        photosController.viewMvc = viewMvc

        return viewMvc.rootView
    }

    override fun onStart() {
        super.onStart()
        photosController.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        photosController.onDestroy()
    }
}
