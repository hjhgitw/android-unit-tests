package com.oliverspryn.blog.androidunittests.dagger

import com.oliverspryn.blog.androidunittests.MainActivity
import com.oliverspryn.blog.androidunittests.dagger.components.DaggerMainComponent
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.dagger.modules.MainModule

object DaggerInjector {

    var mainComponent: MainComponent? = null

    fun buildMainComponent(activity: MainActivity): MainComponent {
        val component = DaggerMainComponent
            .builder()
            .mainModule(MainModule(activity))
            .build()

        mainComponent = component
        return component
    }
}
