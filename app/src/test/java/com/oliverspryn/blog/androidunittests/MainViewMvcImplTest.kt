package com.oliverspryn.blog.androidunittests

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class MainViewMvcImplTest : Spek({

    describe("The MainViewMvcImpl") {

        var mockRootView: View? = null
        var uut: MainViewMvcImpl? = null

        beforeEachTest {
            mockRootView = mock()

            val mockParent: ViewGroup = mock()
            val mockInflater: LayoutInflater = mock {
                on { inflate(R.layout.activity_main, mockParent, false) } doReturn mockRootView
            }

            uut = MainViewMvcImpl(
                inflater = mockInflater,
                parent = mockParent
            )
        }

        describe("when init") {
            it("inflates the layout and sets it as the root view") {
                expect(uut?.rootView).to.equal(mockRootView)
            }
        }
    }
})
