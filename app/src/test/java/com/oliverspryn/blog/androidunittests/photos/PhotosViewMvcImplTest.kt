package com.oliverspryn.blog.androidunittests.photos

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.wrappers.android.LinearLayoutManagerFactory
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class PhotosViewMvcImplTest : Spek({

    describe("The PhotosViewMvcImpl") {

        var mockLinearLayoutManager: LinearLayoutManager? = null
        var mockPhotoList: RecyclerView? = null
        var mockRootView: View? = null

        var uut: PhotosViewMvcImpl? = null

        beforeEachTest {

            val mockContext: Context = mock()
            val mockParent: ViewGroup = mock()
            mockLinearLayoutManager = mock()
            mockPhotoList = mock()

            mockRootView = mock {
                on { context } doReturn mockContext
                on { findViewById<RecyclerView>(R.id.photo_list) } doReturn mockPhotoList
            }

            val mockLinearLayoutManagerFactory: LinearLayoutManagerFactory = mock {
               on { newInstance(mockContext) } doReturn mockLinearLayoutManager!!
            }

            val mockInflater: LayoutInflater = mock {
                on { inflate(R.layout.photos_fragment, mockParent, false) } doReturn mockRootView
            }

            uut = PhotosViewMvcImpl(
                inflater = mockInflater,
                linearLayoutManagerFactory = mockLinearLayoutManagerFactory,
                parent = mockParent
            )
        }

        describe("when init") {

            it("inflates the layout and sets it as the root view") {
                expect(uut?.rootView).to.equal(mockRootView)
            }

            it("obtains a reference to the photo list") {
                expect(uut?.photoList).to.equal(mockPhotoList)
            }

            describe("that list") {
                it("has a linear layout manager") {
                    verify(mockPhotoList)?.layoutManager = mockLinearLayoutManager
                }
            }
        }

        describe("when setAdapter") {

            var mockAdapter: PhotosAdapter? = null

            beforeEachTest {
                mockAdapter = mock()
                uut?.setAdapter(mockAdapter!!)
            }

            it("sets the adapter on the photos list") {
                verify(mockPhotoList)?.adapter = mockAdapter
            }
        }
    }
})
