package com.oliverspryn.blog.androidunittests.mvc

import com.nhaarman.mockito_kotlin.mock
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

object BaseObservableViewMvcTest : Spek({

    describe("The BaseObservableViewMvc") {

        class MockListener

        var uut: BaseObservableViewMvc<MockListener>? = null

        beforeEachTest {
            uut = object : BaseObservableViewMvc<MockListener>() {}
            uut?.rootView = mock()
        }

        describe("when listeners") {

            var result: List<MockListener>? = null

            beforeEachTest {
                uut?.registerListener(MockListener())
                uut?.registerListener(MockListener())
                uut?.registerListener(MockListener())

                result = uut?.listeners
            }

            it("returns a list of all registered listeners") {
                expect(result?.count()).to.equal(3)
            }
        }

        describe("when registerListener") {

            beforeEachTest {
                uut?.registerListener(MockListener())
            }

            it("adds the new listener to the list of registered listeners") {
                val result = uut?.listeners
                expect(result?.count()).to.equal(1)
            }
        }

        describe("when unregisterListener") {

            beforeEachTest {
                val registeredListener = MockListener()

                uut?.registerListener(MockListener())
                uut?.registerListener(MockListener())
                uut?.registerListener(MockListener())
                uut?.registerListener(registeredListener)
                uut?.unregisterListener(registeredListener)
            }

            it("removes the given listener from the list of registered listeners") {
                val result = uut?.listeners
                expect(result?.count()).to.equal(3)
            }
        }
    }
})
