package com.oliverspryn.blog.androidunittests.photos

import android.view.View
import android.view.ViewGroup
import com.nhaarman.mockito_kotlin.doNothing
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.photos.photolistitem.PhotoListItemViewMvc
import com.oliverspryn.blog.androidunittests.wrappers.PhotosFactory
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class PhotosAdapterTest : Spek({

    describe("The PhotosAdapter") {

        var mockListener: PhotoListItemViewMvc.Listener? = null
        var mockPicassoForwarder: PicassoForwarder? = null
        var mockViewMvcFactory: ViewMvcFactory? = null

        val photo1 = PhotosModel(
            id = 42,
            thumbnailUrl = "https://ddg.co/",
            title = "DuckDuckGo",
            url = "https://duckduckgo.com/"
        )

        val photo2 = PhotosModel(
            id = 42,
            thumbnailUrl = "https://ddg.co/",
            title = "DuckDuckGo",
            url = "https://duckduckgo.com/"
        )

        val photos = listOf(photo1, photo2)

        var uut: PhotosAdapter? = null

        beforeEachTest {
            mockListener = mock()
            mockPicassoForwarder = mock()
            mockViewMvcFactory = mock()

            uut = spy(
                PhotosAdapter(
                    listener = mockListener!!,
                    photos = photos,
                    picassoForwarder = mockPicassoForwarder!!,
                    viewMvcFactory = mockViewMvcFactory!!
                )
            )

            doNothing().whenever(uut)?.notifyDataSetChanged()
        }

        describe("when init") {

            it("is initialized with a list of 0 photos") {
                expect(uut?.itemCount).to.equal(0)
            }
        }

        describe("when onBindViewHolder") {

            var mockHolder: PhotosAdapter.ViewHolder? = null

            beforeEachTest {
                val mockViewMvc: PhotoListItemViewMvc = mock()
                mockHolder = mock {
                    on { viewMvc } doReturn mockViewMvc
                }

                uut?.updatePhotos(photos)
                uut?.onBindViewHolder(mockHolder!!, 1)
            }

            it("binds the data to the view MVC in the holder") {
                verify(mockHolder?.viewMvc)?.bindPhoto(photo2)
            }
        }

        describe("onCreateViewController") {

            var mockParent: ViewGroup? = null
            var mockViewMvc: PhotoListItemViewMvc? = null
            var result: PhotosAdapter.ViewHolder? = null

            beforeEachTest {

                mockParent = mock()
                mockViewMvc = mock {
                    on { rootView } doReturn mock<View>()
                }

                whenever(
                    mockViewMvcFactory?.getPhotoListItemViewMvc(
                        mockParent,
                        mockPicassoForwarder!!
                    )
                )
                    .thenReturn(mockViewMvc)

                uut?.updatePhotos(photos)
                result = uut?.onCreateViewHolder(mockParent!!, 0)
            }

            it("creates a view MVC instance") {
                verify(mockViewMvcFactory)?.getPhotoListItemViewMvc(
                    mockParent,
                    mockPicassoForwarder!!
                )
            }

            it("registers the provided listener on the view MVC instance") {
                verify(mockViewMvc)?.registerListener(mockListener!!)
            }

            it("returns a new instance of a view holder") {
                expect(result).to.not.be.`null`
            }
        }

        describe("when getItemCount") {

            var result: Int? = null

            beforeEachTest {
                uut?.updatePhotos(photos)
                result = uut?.itemCount
            }

            it("returns the total number of existing photos") {
                expect(result).to.equal(2)
            }
        }

        describe("when updatePhotos") {

            var resultAfter: Int? = null
            var resultBefore: Int? = null

            beforeEachTest {
                uut?.updatePhotos(photos)
                resultBefore = uut?.itemCount

                reset(uut)
                doNothing().whenever(uut)?.notifyDataSetChanged()

                uut?.updatePhotos(listOf(photo1))
                resultAfter = uut?.itemCount
            }

            it("updates the internal list of photos") {
                expect(resultBefore).to.equal(2)
                expect(resultAfter).to.equal(1)
            }

            it("notifies the list of data set changes") {
                verify(uut)?.notifyDataSetChanged()
            }
        }
    }
})
