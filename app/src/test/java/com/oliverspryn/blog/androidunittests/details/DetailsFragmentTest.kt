package com.oliverspryn.blog.androidunittests.details

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class DetailsFragmentTest : Spek({

    describe("The DetailsFragment") {

        var mockMainComponent: MainComponent? = null
        var uut: DetailsFragment? = null

        beforeEachTest {
            mockMainComponent = mock()

            uut = spy(DetailsFragment(
                mainComponent = mockMainComponent
            ))

            uut?.detailsController = mock()
            uut?.navArgsForwarder = mock()
            uut?.picassoForwarder = mock()
            uut?.viewMvcFactory = mock()
        }

        describe("when init") {

            describe("when there is a MainComponent in the Dagger object graph") {

                it("injects the necessary dependencies") {
                    verify(mockMainComponent)?.inject(any<DetailsFragment>())
                }
            }

            describe("when there isn't a MainComponent in the Dagger object graph") {

                beforeEachTest {
                    mockMainComponent = mock()

                    uut = DetailsFragment(
                        mainComponent = null
                    )
                }

                it("does not inject the necessary dependencies") {
                    verify(mockMainComponent, never())?.inject(any<DetailsFragment>())
                }
            }
        }

        describe("when onCreateView") {

            var mockArguments: Bundle? = null
            var mockContainer: ViewGroup? = null
            var mockDetailsViewMvc: DetailsViewMvc? = null
            var mockModel: PhotosModel? = null
            var mockRootView: View? = null
            var result: View? = null

            beforeEachTest {

                val mockDetailsFragmentArgs: DetailsFragmentArgs = mock()

                mockModel = PhotosModel(
                    id = 42,
                    thumbnailUrl = "https://ddg.co/",
                    title = "DuckDuckGo",
                    url = "https://duckduckgo.com/"
                )

                mockArguments = mock()
                mockContainer = mock()
                mockDetailsViewMvc = mock()
                mockRootView = mock()

                whenever(mockDetailsViewMvc?.rootView).thenReturn(mockRootView)
                whenever(mockDetailsFragmentArgs.photo).thenReturn(mockModel)
                whenever(uut?.navArgsForwarder?.detailsFragmentArgs(mockArguments!!)).thenReturn(mockDetailsFragmentArgs)
                whenever(uut?.viewMvcFactory?.getDetailsViewMvc(mockContainer, uut!!.picassoForwarder)).thenReturn(mockDetailsViewMvc)
            }

            describe("and the arguments on the fragment exists") {

                beforeEachTest {
                    whenever(uut?.arguments).thenReturn(mockArguments)

                    result = uut?.onCreateView(
                        inflater = mock(),
                        container = mockContainer,
                        savedInstanceState = mock()
                    )
                }

                it("sets the view MVC on the controller") {
                    verify(uut?.detailsController)?.viewMvc = mockDetailsViewMvc
                }

                it("calls onCreateView on the controller and it passes it the deserialized arguments") {
                    verify(uut?.detailsController)?.onCreateView(mockModel!!)
                }

                it("returns the root view from that view MVC") {
                    expect(result).to.equal(mockRootView)
                }
            }

            describe("and the arguments on the fragment does not exist") {

                beforeEachTest {
                    whenever(uut?.arguments).thenReturn(null)

                    result = uut?.onCreateView(
                        inflater = mock(),
                        container = mockContainer,
                        savedInstanceState = mock()
                    )
                }

                it("sets the view MVC on the controller") {
                    verify(uut?.detailsController)?.viewMvc = mockDetailsViewMvc
                }

                it("does not call onCreateView on the controller") {
                    verify(uut?.detailsController, never())?.onCreateView(any())
                }

                it("returns the root view from that view MVC") {
                    expect(result).to.equal(mockRootView)
                }
            }
        }

        describe("when onStart") {

            beforeEachTest {
                uut?.onStart()
            }

            it("calls onStart on the controller") {
                verify(uut?.detailsController)?.onStart()
            }
        }

        describe("when onDestroy") {

            beforeEachTest {
                uut?.onDestroy()
            }

            it("calls onDestroy on the controller") {
                verify(uut?.detailsController)?.onDestroy()
            }
        }
    }
})
